package clases;

import java.time.LocalDate;



public class Festival {

	
	//creacion del array 
	private Cantante[] cantantes;
	
	//creacion de constructor
	public Festival(int maxCantantes) {
		
		this.cantantes = new Cantante[maxCantantes];
		
		
		}
	
	
	//creacion del metodo para dar de alta a los cantantes (Principal)
	public void altaCantantes ( String nombre, double precio, String genero_musical, LocalDate fecha, String codCantante) {
		for (int i=0; i<cantantes.length; i++) {
			
			if (cantantes[i]== null) {
			cantantes[i]=new Cantante(nombre);
			cantantes[i].setPrecio(precio);
			cantantes[i].setGenero_musical(genero_musical);
			cantantes[i].setFecha(fecha);
			cantantes[i].setCodCantante(codCantante);
			break;
			}
		}
	}
	
	//creacion de metodo  para buscar
	
	public Cantante buscarCantante (String nombre) {
		for (int i=0; i<cantantes.length; i++ ) {
			if (cantantes[i] != null) {
				if(cantantes[i].getNombre().equals(nombre)) {
					System.out.println(cantantes[i]);
					return cantantes[i];
					
				}
			}
		}
		
		return null;
		
	}
	
	//creacion de metodo para eliminar cantante

	public void eliminarCantante(String nombre) {
		
		for (int i =0; i<cantantes.length;i++) {
			if (cantantes[i]!= null) {
				
				if (cantantes[i].getNombre().equals(nombre)) {
					cantantes[i]=null;
				}
			}
			
		}
		
	}
	
	//creacion de metodo  para listar a los cantantes
	public void listarCantantes() {
		for (int i = 0; i < cantantes.length; i++) {
			if (cantantes[i] != null) {
				System.out.println(cantantes[i]);
			}
		}
	}
	
	//creacion de metodo para listar cantantes por genero musical 
	public void listarCantantesGenero (String genero_musical) {
		for (int i=0; i<cantantes.length; i++) {
			if (cantantes[i]!=null) {
				if (cantantes[i].getGenero_musical().equals(genero_musical)) {
					System.out.println(cantantes[i]);
				}
			}
			
		}

	}
	
	//cracion de metodo para listar por codigo de cantante
	public void listarCantantesCodigo (String codCantante) {
		for (int i=0; i<cantantes.length; i++) {
			if (cantantes[i] !=null) {
				if(cantantes[i].getCodCantante().equals(codCantante)) {
					System.out.println(cantantes[i]);
				}
			}
			
		}
		
	}
	
	
	//creacion de metodo para listar  los cantantes que su precio sea superior a 30
	
	public void listarPrecio (double precio) {
		for (int i=0; i<cantantes.length; i++) {
			if (cantantes[i] != null) {
				if (cantantes[i].getPrecio() >30) {
					System.out.println(cantantes[i]);
				}
			}
		}
	}
	
	//creacion de nuevo cantante

	public void NuevoCantante ( String nombre, double precio, String genero_musical, LocalDate fecha, String codCantante) {
		for (int i=0; i<cantantes.length; i++) {
			
			if (cantantes[i]== null) {
			cantantes[i]=new Cantante(nombre);
			cantantes[i].setPrecio(precio);
			cantantes[i].setGenero_musical(genero_musical);
			cantantes[i].setFecha(fecha);
			cantantes[i].setCodCantante(codCantante);
			break;
			}
		}
	}
	
	//creacion de metodo para editar el nombre de un cantante
	
	public void cambiarNombreCantante(String nombre, String nombre2) {
		for (int i = 0; i < cantantes.length; i++) {
			if (cantantes[i] != null) {
				if (cantantes[i].getNombre().equals(nombre)) {
					cantantes[i].setNombre(nombre2);
				}
			}
		}
	}
	
	
	}
	
	
	
	
	
	
	
	
	
	