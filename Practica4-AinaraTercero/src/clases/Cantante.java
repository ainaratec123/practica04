package clases;

import java.time.LocalDate;
public class Cantante {


	//Creaci�n de atributos 
	private String nombre;
	private double precio;
	private String genero_musical;
	private LocalDate fecha;
	private String codCantante;
	
	
	//Creacion de constructor 
	
	public Cantante(String nombre) {
		this.nombre = nombre;
		
		
	}
	
	//creacion de getter y setter
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getGenero_musical() {
		return genero_musical;
	}
	public void setGenero_musical(String genero_musical) {
		this.genero_musical = genero_musical;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	public String getCodCantante() {
		return codCantante;
	}
	public void setCodCantante(String codCantante) {
		this.codCantante = codCantante;
	}
	
	
	//creacion de toString 
	@Override
	public String toString() {
		return "Cantantes [nombre=" + nombre + ", precio=" + precio + ", genero_musical=" + genero_musical + ", fecha="
				+ fecha + ", codCantante=" + codCantante + "]";
	}
	
	
}
