package programa;

import java.time.LocalDate;
import java.util.Scanner;

public class Programa {
	//Creacion de escaner que usara el programa
	static Scanner escaner = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			//creacion del maximo de cantntes
			int maxCantantes= 4;
			
			
			//creacion de
			clases.Festival miFestival = new clases.Festival(maxCantantes);
			
			
			//creacion de variable opcion
			int opcion;
		
			//creacion con bucle y menu 
			do {
			System.out.println("**********MENU******************");
			System.out.println("1. Dar de alta a cantantes ");
			System.out.println("2. Buscar cantante");
			System.out.println("3. Eliminar cantante");
			System.out.println("4. Listar general cantante");
			System.out.println("5. Cambiar cantante");
			System.out.println("6. Listar por algun atributo");
			System.out.println("7. Introducir nuevo cantante");
			System.out.println("8. Salir");
			System.out.println("********************************");
			System.out.println("Introduce una opcion:");
			opcion = escaner.nextInt();
			escaner.nextLine();
			
			//creacion se switch 
			switch(opcion) {
			
				//caso1 Introducir datos para darlos de alta
				case 1:
				for (int i=0; i<maxCantantes; i++) {
					System.out.println("Dar de alta a los 4 cantantes");	
					System.out.println("Introduce el nombre:");
					String nombre = escaner.nextLine();
					System.out.println("Introduce el precio del cantante:");
					double precio = escaner.nextDouble();
					escaner.nextLine();
					System.out.println("Introduce el genero musical del cantante:");
					String genero_musical = escaner.nextLine();
					System.out.println("La fecha");
					LocalDate fecha = LocalDate.now();
					System.out.println("Introduce el codCantante:");
					String codCantante = escaner.nextLine();
					
				//llamar metodo
				miFestival.altaCantantes(nombre, precio, genero_musical,fecha, codCantante);
				}
				
				break;
				
				//caso2 Busqueda de datos
				case 2:
					System.out.println("Has selecionado buscar");
					System.out.println("Introduce el nombre que quieres buscar:");
					String nombreBuscado= escaner.nextLine();
					//llamamos al metodo
					miFestival.buscarCantante(nombreBuscado);
				break;
				
				//caso3 eliminar dato
				case 3:
					System.out.println("Has selecionado la opcion de eliminar ");
					System.out.println("Introduce el nombre del cantante que quieras eliminar:");
					String nombreEliminar = escaner.nextLine();
					//llamar metodo
					miFestival.eliminarCantante(nombreEliminar);
				break;
				//caso 4 Listar cantantes
				case 4:
					System.out.println("Has selecionado listar los cantantes ");
					//llamar metodo
					miFestival.listarCantantes();
				break;
				//caso 5 Editar nombre de cantantes
				case 5:
					System.out.println("Has selecionado cambiar cantante ");
					System.out.println("Introduce el nombre que quieras cambiar");
					String nombreOriginal = escaner.nextLine();
					System.out.println("Introduce el nombre nuevo:");
					String nombreCantanteNuevo = escaner.nextLine();
					//llamar metodo
					miFestival.cambiarNombreCantante(nombreOriginal, nombreCantanteNuevo);
				break;
				//caso 6 Listar por atributos 
				case 6:
					//variable opcion 2
					int opcion1;
					
					//creacion de menu 
					do{
					System.out.println("Has selecionado mostrar por atributo");
					System.out.println("1- Listar por genero musical");
					System.out.println("2- Listar por codCantante");
					System.out.println("3- Listar cantantes que su precio es mayor de 30");
					System.out.println("4- Salir");
					System.out.println("Introduce una opcion:");
					opcion1 = escaner.nextInt();
					
					//creacion de switch 
					switch (opcion1) {
					//caso 1 submenu, listar por genero
					case 1:
						System.out.println("Has selecionado a listar cantates por genero");
						System.out.println("Introduce el genero musical que quieras que se listen:");
						escaner.nextLine();
						String genero = escaner.nextLine();
						//llamar metodo
						miFestival.listarCantantesGenero(genero);
						
						
					break;
					//caso 2 submenu listar por codigo de cantante
					case 2:
						System.out.println("Has selecionado a listar cantates por codigo");
						System.out.println("Introduce el codigo que quieras que se listen:");
						escaner.nextLine();
						String codigo = escaner.nextLine();
						//llamar metodo
						miFestival.listarCantantesCodigo(codigo);
						
					
					break;
					
					
					case 3:
						//caso 3 submenu listar cantante mayores de 30 (precio)
						System.out.println("Has selecionado listar los cantantes por precio mayor de 30");
						//llamar metodo
						miFestival.listarPrecio(30);
					break;
					//opcion de salir 
					case 4:
						
						System.out.println("Has selecionado la opcion de salir");
						
					break;
					
				
					//caso alternativo
					default:
						System.out.println("No has introducido una opcion valida");
					
					}
					//cerrar bucle
					}while(opcion1!=4);
				break;
				
				
				//Introducir nuevo cantante
				case 7:
					System.out.println("Has selecionado introducir nueo cantante");
					System.out.println("Introduce el nombre:");
					String nombreNuevo = escaner.nextLine();
					System.out.println("Introduce el precio del cantante:");
					double precioNuevo = escaner.nextDouble();
					escaner.nextLine();
					System.out.println("Introduce el genero musical del cantante:");
					String genero_musicalNuevo = escaner.nextLine();
					System.out.println("La fecha");
					LocalDate fechaNuevo = LocalDate.now();
					System.out.println("Introduce el codCantante:");
					String codCantanteNuevo = escaner.nextLine();
					//llamar metodo
					miFestival.NuevoCantante(nombreNuevo, precioNuevo, genero_musicalNuevo, fechaNuevo, codCantanteNuevo);
				break;
				//Opcion de salir menu principal
				
				case 8:
					System.out.println("Has selecionado la opcion de salir");
					System.exit(0);
					
				break;
				//caso alternativo
				default: 
				 System.out.println("No has selecionado una opcion valida");
			}
			//cerramos bucle
			}while(opcion!=8);
			
			
			
			
		
	}
	
	

}
